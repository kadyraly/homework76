
import axios from '../../axios-api';
import {CREATE_CHAT_SUCCESS, FETCH_CHATS_SUCCESS} from "./actionTypes";


export const fetchChatsSuccess = chats => {
    return {type: FETCH_CHATS_SUCCESS, chats};
};

export const fetchChats = () => {
    return dispatch => {
        axios.get('/chats').then(
            response => dispatch(fetchChatsSuccess(response.data))
        );

    };
};

export const createChatSuccess = () => {
    return {type: CREATE_CHAT_SUCCESS};
};

export const createChat = chatData => {
    return dispatch => {
        return axios.post('/chats', chatData).then(
            response => dispatch(createChatSuccess())
        );
    };
};