import React, {Component} from 'react';
import './ChatForm.css';

class ChatForm extends Component {
    state = {
        author: '',
        message: ''
    };

    submitFormHandler = event => {
        event.preventDefault();
        this.props.onSubmit(this.state);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    render() {
        return (
            <form action="" className="ChatForm" onSubmit={this.submitFormHandler}>
                <div>
                    <label>Author:</label>
                    <input
                        type="text"
                        value={this.state.author}
                        placeholder="author here..."
                        className="form-control"
                        name="author"
                        onChange={this.inputChangeHandler}
                        required
                    />
                </div>
                <div>
                    <label>Message:</label>
                    <textarea
                        name="message"
                        value={this.state.message}
                        placeholder="message"
                        className="form-control"
                        onChange ={this.inputChangeHandler}
                    />
                </div>
                <div>
                    <button type="submit">Send</button>
                </div>
            </form>
        );
    }
}

export default ChatForm;
