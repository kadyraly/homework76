import React, {Component, Fragment} from 'react';
import {Route, Switch} from "react-router-dom";
import Chats from "./container/Chats/Chats";
import NewChat from "./container/NewChat/NewChat";
import Header from "./component/UI/Header/Header";

class App extends Component {
    render() {
        return (
            <Fragment>
               <Header/>
                <main className="container">
                    <Switch>
                        <Route path="/" exact component={Chats}/>
                        <Route path="/chats/new" exact component={NewChat}/>
                    </Switch>
                </main>
            </Fragment>
        );
    }
}

export default App;

