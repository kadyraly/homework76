import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";
import {fetchChats} from "../../store/action/chat";
import './Chats.css';

class Chats extends Component {

    componentDidMount() {
        this.props.onFetchChats();
    }
    render() {
        return (
            <div className="Chats">
                <h1>Chats</h1>
                {this.props.chats.map(chat => (
                    <div key={chat.id}>
                        <Link to={'/chats/' + chat.id}>
                            <h3>{chat.author}</h3>
                            <p> {chat.message}</p>
                        </Link>

                    </div>
                ))}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
       chats: state.chats.chats
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchChats: () => dispatch(fetchChats())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Chats);