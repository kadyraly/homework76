const express = require('express');
const router = express.Router();

const createRouter = (db) => {

    router.get('/', (req, res) => {
        res.send(db.getData(req.query.datetime));
    });


    router.post('/', (req, res) => {
        const chat = req.body;
        if (!chat.author || !chat.message) {
            res.status(400)
                .send({error: "Author and message must be present in the request"})
        } else {
            db.addItem(chat).then(result => {
                res.send(result);
            });
        }

    });


    router.get('/:id', (req, res) => {
        res.send(db.getDataById(req.params.id));
    });

    return router;
};

module.exports = createRouter;