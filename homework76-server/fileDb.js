const fs = require('fs');
const nanoid = require("nanoid");

let data = null;


module.exports = {
    init: () => {
        return new Promise((resolve, reject) => {
            fs.readFile('./db.json', (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    data = JSON.parse(result);
                    resolve(data);
                }
            });
        });
    },
    getData: (request) => {
        if (request) {
            let index = data.findIndex((date) => request === date.datetime);
            return data.slice(index + 1);
        } else {
            return data.slice(-30)
        }
    },
    getDataById: id => {
        return data.find(item => item.id === id);
    },
    addItem: (item) => {
        item.id = nanoid();
        item.datetime = new Date().toISOString();
        data.push(item);

        let contents = JSON.stringify(data);

        return new Promise((resolve, reject) => {
            fs.writeFile('./db.json', contents, err => {
                if (err) {
                    reject(err);
                } else {
                    resolve(item);
                }
            });
        });
    }
};